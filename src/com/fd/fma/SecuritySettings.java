package com.fd.fma;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Locale;

import android.content.res.Configuration;
import android.os.SystemClock;
import android.widget.LinearLayout;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.core.UiWatcher;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;


	//所有UI自动测试都要继承UiAutomatorTestCase
	public class SecuritySettings extends UiAutomatorTestCase {
		private void updateLanguage(Locale locale) {
	        try {
	            Object objIActMag;
	            Class<?> clzIActMag = Class.forName("android.app.IActivityManager");
	            Class<?> clzActMagNative = Class.forName("android.app.ActivityManagerNative");
	            Method mtdActMagNative$getDefault = clzActMagNative.getDeclaredMethod("getDefault");
	            // IActivityManager iActMag = ActivityManagerNative.getDefault();
	            objIActMag = mtdActMagNative$getDefault.invoke(clzActMagNative);
	            // Configuration config = iActMag.getConfiguration();
	            Method mtdIActMag$getConfiguration = clzIActMag.getDeclaredMethod("getConfiguration");
	            Configuration config = (Configuration) mtdIActMag$getConfiguration.invoke(objIActMag);
	            config.locale = locale;
	            // iActMag.updateConfiguration(config);
	            // 此处需要声明权限:android.permission.CHANGE_CONFIGURATION
	            // 会重新调用 onCreate();
	            Class<?>[] clzParams = { Configuration.class };
	            Method mtdIActMag$updateConfiguration = clzIActMag.getDeclaredMethod(
	                    "updateConfiguration", clzParams);
	            mtdIActMag$updateConfiguration.invoke(objIActMag, config);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
		
		public void testRemoveGoogleAccount() throws UiObjectNotFoundException{
			UiWatcher closeAlertWatcher = new UiWatcher() {
				   @Override
				   public boolean checkForCondition() {
				      UiObject selectAll = new UiObject(new UiSelector().descriptionContains("SELECT　ALL"));
				      UiObject cut = new UiObject(new UiSelector().descriptionContains("CUT"));
				      System.out.println("findevent");
				      if(selectAll.waitForExists(50)||cut.waitForExists(50))
						try {
							if(selectAll.exists())
								selectAll.click();
							if(cut.exists())
								cut.click();
						} catch (UiObjectNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				        
				      return false;
				   }
				};
			UiDevice.getInstance().registerWatcher("CLOSE_ALERT_WATCHER", closeAlertWatcher);
			UiDevice.getInstance().runWatchers();
			UiObject obj = new UiObject(new UiSelector().className(android.widget.EditText.class.getName()));
			//obj.clearTextField();
			obj.longClick();
			obj.setText("4084757608");
			UiDevice.getInstance().removeWatcher("CLOSE_ALERT_WATCHER");
		}
		
		public void testSetEnglish() throws UiObjectNotFoundException{
			updateLanguage(Locale.ENGLISH);
			sleep(1000);
		}
		
		public void testOemUnlock() throws UiObjectNotFoundException{
			updateLanguage(Locale.ENGLISH);
			sleep(1000);

			//获取UiDevice对象
			UiDevice device = getUiDevice();

			device.pressBack();
			
			String sError="";
			String sErrorAll = "";
			try{
				Process p = Runtime.getRuntime().exec("am start -n com.android.settings/.Settings$DevelopmentSettingsActivity -f 67108864");
				BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				while ((sError = stdError.readLine()) != null) {
					System.out.println(sError);
					sErrorAll+=sError;
				}
				if(sErrorAll!=null && !sErrorAll.isEmpty())
				{
					System.out.println("error=runDevelopmentSettingsActivity\n");
					return;
				}
			}catch (IOException e1) {
				System.out.println("exception=runDevelopmentSettingsActivity\n");
				return;
			}
			sleep(2000);
			UiScrollable itemViews = new UiScrollable(new UiSelector().scrollable(true));
	        itemViews.setAsVerticalList();//.setAsHorizontalList();
			
	        UiObject barlae=itemViews.getChild(new UiSelector().textMatches("OEM unlock.*"));
	        if(!barlae.exists()){
	        	System.out.print("oemunlock=no\n");
	        	return;
	        }
	        try{
	        	barlae = itemViews.getChildByText(new UiSelector().className(LinearLayout.class.getName()),"OEM unlocking", true);
	        }catch(Exception e){
	        	try{
	        		barlae = itemViews.getChildByText(new UiSelector().className(LinearLayout.class.getName()),"OEM unlock", true);
	        	}catch(Exception e1){
	        		System.out.print("oemunlock=no\n");
	        	}
	        }
			if(!barlae.exists())
			{
				String spackname = device.getCurrentPackageName();
				if("com.google.settings".compareToIgnoreCase(spackname)!=0)
				{
					System.out.print("error=retry\n");
					device.pressBack();
				}else{
					System.out.print("oemunlock=no\n");
				}				
				return;
			}
			UiObject btSwitch1 = barlae.getChild(new UiSelector().classNameMatches(".*?Switch"));
			if(btSwitch1.exists())
			{
				Boolean barlaecheck = btSwitch1.isChecked();
		        if(barlaecheck){
		        	System.out.print("oemunlock=true\n");
		        }else{
		        	System.out.print("oemunlock=false\n");
		        }
	        }else
	        {
	        	System.out.println("error=notfindcontroller\n");
	        }
		}
		public void testDemo() throws UiObjectNotFoundException{
			
//			AccountManager accountManager = AccountManager.get();
//	        Account[] accounts = accountManager.getAccountsByType("com.google");
			
			updateLanguage(Locale.ENGLISH);
			sleep(1000);

			//获取UiDevice对象
			UiDevice device = getUiDevice();

			device.pressBack();

			
			String sError="";
			String sErrorAll = "";
			try {
				//if(Build.VERSION.SDK_INT<23)
				{
					Process p = Runtime.getRuntime().exec("am start -n com.google.android.gms/.app.settings.GoogleSettingsActivity -f 67108864");
					BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
					while ((sError = stdError.readLine()) != null) {
						System.out.println(sError);
						sErrorAll+=sError;
					}
				}
				if(sErrorAll!=null && !sErrorAll.isEmpty())
				{	
					System.out.println("new method run google settings.");
					Process pp = Runtime.getRuntime().exec("am start -n com.google.android.gms/.app.settings.GoogleSettingsLink -f 67108864");
					BufferedReader stdError1 = new BufferedReader(new InputStreamReader(pp.getErrorStream()));
					while ((sError = stdError1.readLine()) != null) {
						System.out.println(sError);
					}					
				}
				sleep(5000);
			} catch (IOException e1) {
				System.out.println("can not find Google Settings.run exception");
				try {   
					FileWriter fileWriter = new FileWriter("/data/local/tmp/testresult.xml", false);
			    	BufferedWriter oos = new BufferedWriter(fileWriter);
			    	oos.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n");
			    	oos.write("<lock>\r\n");
			    	oos.write("<fmp>failed</fmp>\r\n");
			    	oos.write("</lock>");
					oos.close();
				} catch (Exception e) {		
					e.printStackTrace();
				}
				
				return;
			}
			
/*			device.pressBack();
			device.pressBack();
			device.pressBack();
			device.pressBack();
			
			int nTime = 0;
			while(true)
			{	//点击home键操作
				device.pressBack();
				device.pressHome();
				
				//如截图中提到的，我们使用content-desc属性Apps选择所有应用列表
				UiObject appsTab = new UiObject(new UiSelector().description("Apps"));
				if(!appsTab.exists())
				{
					System.out.println("can not find apps.");
				}
				appsTab.clickAndWaitForNewWindow();
				
				//因为短信界面在第二个列表页面，所以我们可以先滑动一下
				//通过scrollable属性来选定滑动view
				UiScrollable appViews = new UiScrollable(new UiSelector().scrollable(true));
				appViews.setAsHorizontalList();
				
				
				//appViews.scrollForward();			
				//通过类名和Text找到短信应用图标，Text获取通过uiautomatorviewer
				Boolean bscrll = true;
				UiObject messageApp = appViews.getChild(new UiSelector().descriptionMatches("Google Settings"));
				while(!messageApp.exists())
				{
					if (bscrll)
					{
						bscrll = appViews.scrollForward();
					}
					else
					{
						if (!appViews.scrollBackward())
							break;
					}
//					sleep(1000);
				}
				
				if(!messageApp.exists())
				{
					System.out.println("can not find Google Settings.");
					try {   
						FileWriter fileWriter = new FileWriter("/data/local/tmp/testresult.xml", false);
				    	BufferedWriter oos = new BufferedWriter(fileWriter);
				    	oos.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n");
				    	oos.write("<lock>\r\n");
				    	oos.write("<support>false</support>\r\n");
				    	oos.write("</lock>");
						oos.close();
					} catch (Exception e) {		
						e.printStackTrace();
					}
				}
				
				//点击并等待打开短信应用
				messageApp.clickAndWaitForNewWindow();
//				sleep(1000);
				
				if (nTime == 0)
				{
					while (device.pressBack())
					{
						System.out.println("key back.");
						if(messageApp.exists()) break;
					}
				}
				nTime ++;
				if (nTime > 1) break;
			}*/
			Boolean bEr = (sError !=null && sError.length()>0);
			if(bEr)
			{
				try {   
					FileWriter fileWriter = new FileWriter("/data/local/tmp/testresult.xml", false);
			    	BufferedWriter oos = new BufferedWriter(fileWriter);
			    	oos.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n");
			    	oos.write("<lock>\r\n");
			    	oos.write("<fmp>notsupport</fmp>\r\n");
			    	oos.write("</lock>");
					oos.close();
				} catch (Exception e) {		
					e.printStackTrace();
				}
				return;
			}
			UiScrollable googlesettings = new UiScrollable(new UiSelector().packageName("com.google.android.gms").classNameMatches(".*?RecyclerView|.*?ScrollView"));
			if((!googlesettings.exists())&& !bEr)
			{
				device.pressBack();
			}
			if(!googlesettings.exists())
			{
				try {   
					FileWriter fileWriter = new FileWriter("/data/local/tmp/testresult.xml", false);
			    	BufferedWriter oos = new BufferedWriter(fileWriter);
			    	oos.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n");
			    	oos.write("<lock>\r\n");
			    	oos.write("<fmp>failed</fmp>\r\n");
			    	oos.write("</lock>");
					oos.close();
				} catch (Exception e) {		
					e.printStackTrace();
				}
				System.out.println("can not find google settings.");
				return;
			}
			
			if (googlesettings.scrollIntoView(new UiSelector().text("Security")))
			{
				UiObject security = googlesettings.getChild(new UiSelector().text("Security"));
				if(!security.exists())
				{
					System.out.println("can not find Security.");
					String spackname = device.getCurrentPackageName();
					if("com.google.android.gms".compareToIgnoreCase(spackname)!=0)
					{
						device.pressBack();
					}
				}
				if(security.exists())
					security.clickAndWaitForNewWindow();
				else
				{
					try {   
						FileWriter fileWriter = new FileWriter("/data/local/tmp/testresult.xml", false);
				    	BufferedWriter oos = new BufferedWriter(fileWriter);
				    	oos.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n");
				    	oos.write("<lock>\r\n");
				    	oos.write("<fmp>failed</fmp>\r\n");
				    	oos.write("</lock>");
						oos.close();
					} catch (Exception e) {		
						e.printStackTrace();
					}
					return;
				}
				//sleep(1000);
			}

			UiScrollable googlefmp = new UiScrollable(new UiSelector().packageName("com.google.android.gms").classNameMatches(".*?RecyclerView|.*?ScrollView"));
			if(!googlefmp.exists())
			{
				String spackname = device.getCurrentPackageName();
				if("com.google.android.gms".compareToIgnoreCase(spackname)!=0)
				{
					device.pressBack();
				}else
				{
					sleep(1000);
					if(!googlefmp.exists())
					{
						try {   
							FileWriter fileWriter = new FileWriter("/data/local/tmp/testresult.xml", false);
					    	BufferedWriter oos = new BufferedWriter(fileWriter);
					    	oos.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n");
					    	oos.write("<lock>\r\n");
					    	oos.write("<fmp>failed</fmp>\r\n");
					    	oos.write("</lock>");
							oos.close();
						} catch (Exception e) {		
							e.printStackTrace();
						}
						System.out.println("Remotely locate this device");
						return;
					}
				}
			}

			try{
				UiObject barlae = googlefmp.getChildByText(new UiSelector().className(LinearLayout.class.getName()),"Allow remote lock and erase", true);
				if(!barlae.exists())
				{
					String spackname = device.getCurrentPackageName();
					if("com.google.android.gms".compareToIgnoreCase(spackname)!=0)
					{
						device.pressBack();
					}
				}
				UiObject btSwitch1 = barlae.getChild(new UiSelector().className(android.widget.Switch.class.getName()));
			//btSwitch.click();
				Boolean barlaecheck = btSwitch1.isChecked();
	
				UiObject btItem = googlefmp.getChildByText(new UiSelector().className(LinearLayout.class.getName()),"Remotely locate this device", true);
	
				UiObject btSwitch = btItem.getChild(new UiSelector().className(android.widget.Switch.class.getName()));
				//btSwitch.click();
				Boolean bRltdcheck = btSwitch.isChecked();
				
				try {   
					FileWriter fileWriter = new FileWriter("/data/local/tmp/testresult.xml", false);
			    	BufferedWriter oos = new BufferedWriter(fileWriter);
			    	oos.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n");
			    	oos.write("<lock>\r\n");
			    	oos.write("<fmp>"+barlaecheck.toString()+"</fmp>\r\n");
			    	oos.write("<fmc>"+bRltdcheck.toString()+"</fmc>\r\n");
			    	oos.write("</lock>");
					oos.close();
				} catch (Exception e) {		
					e.printStackTrace();
				}
			}catch(Exception e){
				e.printStackTrace();
				try {   
					FileWriter fileWriter = new FileWriter("/data/local/tmp/testresult.xml", false);
			    	BufferedWriter oos = new BufferedWriter(fileWriter);
			    	oos.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n");
			    	oos.write("<lock>\r\n");
			    	oos.write("<fmp>failed</fmp>\r\n");
			    	oos.write("</lock>");
					oos.close();
				} catch (Exception ee) {		
					ee.printStackTrace();
				}
			}
		}
	}
